<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 11.10.2018
 * Time: 16:34
 */
ini_set('memory_limit', '1024M');
require_once('autoload.php');

use lib\Client;

$client_1 = new Client('Коля');
$client_2 = new Client('Вася');


$client_1->generateSecretNum();
$client_2->generateSecretNum();

$client_1->calcPublicNum();
$client_2->calcPublicNum();

// Момент обмена ключами
$client_1->sendPublicNum($client_2);
$client_2->sendPublicNum($client_1);

$client_1->calcFinallyKey();
$client_2->calcFinallyKey();
