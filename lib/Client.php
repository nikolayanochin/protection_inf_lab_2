<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 08.11.2018
 * Time: 18:46
 */


namespace lib;

/**
 * Class Client
 * @package lib
 */
class Client extends ClientHelper
{
    private $name;
    private $secret_num;
    private $public_num;
    private $client_public_num;
    private $finally_key;

    /**
     * Client constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }


    /**
     * Генерируем секретный ключ
     */
    public function generateSecretNum()
    {
        $this->secret_num = rand(1, $this->server_P);
    }

    /**
     * Расчтваем публичный ключ
     */
    public function calcPublicNum()
    {
        $this->public_num = ($this->server_A ^ $this->secret_num) % $this->server_P;
    }

    /**
     * Передаем публичный ключ кленту
     * @param Client $client
     */
    public function sendPublicNum(Client $client)
    {
        $client->setPublicNum($this->public_num);
    }

    /**
     * Устанавлваем публичный ключ другого клиента
     * @param $num
     */
    private function setPublicNum($num)
    {
        $this->client_public_num = $num;
    }

    /**
     * Расчитываем 
     */
    public function calcFinallyKey()
    {
        $this->finally_key = ($this->client_public_num ^ $this->secret_num) % $this->server_P;
        echo $this->name . ' посчитал ключ: ' . $this->finally_key . "\n";
    }

}